create table teacher
(
    id integer identity not null,
    first_name varchar(255),
    last_name varchar(255),
    subject varchar(255),
    primary key(id)
);