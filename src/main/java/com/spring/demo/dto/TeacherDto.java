package com.spring.demo.dto;

public class TeacherDto {

    private int id;
    private String firstName;
    private String lastName;
    private String subject;

    public TeacherDto() {
    }

    public TeacherDto(int id, String firstName, String lastName, String subject) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.subject = subject;
    }

    public int getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getSubject() {
        return subject;
    }
}
