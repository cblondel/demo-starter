package com.spring.demo.controller.api;

import com.spring.demo.exception.EntityNotFoundException;
import com.spring.demo.dto.ErrorDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class ExceptionApiTranslator {

    private static final Logger logger = LoggerFactory.getLogger(ExceptionApiTranslator.class);

    @ExceptionHandler(EntityNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ResponseBody
    public ErrorDto handleNotFoundException(Exception ex) {
        return new ErrorDto(ex.getMessage());
    }
}
