package com.spring.demo.controller.api;

import com.spring.demo.service.TeacherService;
import com.spring.demo.dto.TeacherDto;
import com.spring.demo.dto.TeachersDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class TeacherController extends AbstractApiController{

    @Autowired
    private TeacherService teacherService;

    @RequestMapping(value="/teachers", method = RequestMethod.GET)
    @ResponseBody
    public TeachersDto getTeachers() {
        return teacherService.findAll();
    }

    @RequestMapping(value="/teacher/{id}", method = RequestMethod.GET)
    @ResponseBody
    public TeacherDto getTeacher(@PathVariable int id) {
        return teacherService.FindById(id);
    }
}
