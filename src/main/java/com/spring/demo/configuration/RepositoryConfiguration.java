package com.spring.demo.configuration;

import com.spring.demo.repository.TeacherRepository;
import com.spring.demo.repository.jdbc.JdbcTeacherRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RepositoryConfiguration {

//    @Bean
//    public TeacherRepository teacherRepository() {
//        return new JdbcTeacherRepository();
//    }
}
