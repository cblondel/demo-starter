package com.spring.demo.repository.inmemory;

import com.spring.demo.model.TeacherModel;
import com.spring.demo.repository.TeacherRepository;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Repository;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Repository
@ConditionalOnProperty(name = "demo.repository", havingValue = "mem")
public class InMemoryTeacherRepository implements TeacherRepository {

    private static final List<TeacherModel> teachers = Arrays.asList(
            new TeacherModel(1, "Thierry", "Fricheteau", "Gestion de projet"),
            new TeacherModel(2, "Alain", "Husson", "Anglais"),
            new TeacherModel(3, "Amandine", "Leriche", "Maths"),
            new TeacherModel(4, "Philippe", "Kubiak", "Informatique"),
            new TeacherModel(5, "Maxime", "Ogier", "Informatique")
    );

    @Override
    public List<TeacherModel> findAll() {
        return teachers;
    }

    @Override
    public TeacherModel findById(int id) {
        return teachers.stream()
                .filter(teacherModel -> teacherModel.getId() == id)
                .findFirst()
                .orElse(null);
    }

    public static void main(String... args) {
        System.out.println("---------- findByIdWithForEach -------------");
        System.out.println(findByIdWithForEach(1));
        System.out.println("---------- findUniqSubjects -------------");
        List<String> l = findUniqSubjects();
        for(String s : l){
            System.out.println(s);
        }
        System.out.println("---------- findSortedTeachers -------------");
        List<TeacherModel> l1 = findSortedTeachers();
        for(TeacherModel s : l1){
            System.out.println(s);
        }
        System.out.println("---------- sumIds -------------");
        System.out.println(sumIds());
    }

    public static TeacherModel findByIdWithForEach(int id) {
        for(TeacherModel teacherModel : teachers) {
            if(id == teacherModel.getId()){
                return teacherModel;
            }
        }
        return null;
    }

    public static List<String> findUniqSubjects() {
        return teachers.stream()
                .map(TeacherModel::getSubject)
                .distinct()
                .collect(Collectors.toList());
    }

    public static List<TeacherModel> findSortedTeachers() {
        /*return teachers.stream()
                .sorted(
                        Comparator
                                .comparing(TeacherModel::getLastName)
                                .thenComparing(TeacherModel::getFirstName)
                )
                .collect(Collectors.toList());*/

        Collections.sort(teachers, new Comparator<TeacherModel>() {
            @Override
            public int compare(TeacherModel o1, TeacherModel o2) {
                return o1.getFirstName().compareTo(o2.getFirstName());
            }
        });

        //Collections.sort(teachers, (o1, o2) -> o1.getLastName().compareTo(o2.getLastName()));

        return teachers;
    }

    public static int sumIds() {
        return teachers.stream()
                .map(TeacherModel::getId)
                .reduce(0, (total, id) -> total + id);
    }
}
