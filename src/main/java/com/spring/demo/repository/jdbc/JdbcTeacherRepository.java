package com.spring.demo.repository.jdbc;

import com.spring.demo.model.TeacherModel;
import com.spring.demo.repository.TeacherRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@ConditionalOnProperty(name = "demo.repository", havingValue = "jdbc")
public class JdbcTeacherRepository implements TeacherRepository {

    private static final String FIND_ALL_QUERY = "SELECT * FROM TEACHER;";
    private static final String FIND_BY_ID_QUERY = "SELECT * FROM TEACHER WHERE ID = ?;";

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public List<TeacherModel> findAll() {
        return jdbcTemplate.query(FIND_ALL_QUERY, new TeacherRowMapper());
    }

    @Override
    public TeacherModel findById(int id) {
        try {
            return jdbcTemplate.queryForObject(FIND_BY_ID_QUERY, new Object[]{id}, new TeacherRowMapper());
        } catch (EmptyResultDataAccessException ex) {
            return null;
        }
    }
}
